# NodeJS Projects #

### What is this repository for? ###

* This repository contains tutorials for exercising in Node from the book Web Development with NodeJS and my personal projects in NodeJS using Express framework.
* Ver. 1.0

### Tutorials projects ###
* First Tutorial - simple pure nodejs web-server displaying Hello World in Browser.
* Second Tutorial - more complicated web-server with Home, About and Error pages.
* Third Tutorial - serving static resourses with pure NodeJS.

### Sample Project ###
* QA
* 1) Page Testing: Global and Page Specific using Mocha and Chai npm install --save-dev mocha chai
* 2) Cross Page Testing: using Zombie npm install --save-dev zombie
* 3) Logical Testing (Unit testing);
