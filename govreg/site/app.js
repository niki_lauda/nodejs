var express = require('express');
var handlebars = require('express3-handlebars').create({defaultLayout:'main'});
var app = express();

//SERVER CONFIGURATION
app.engine('handlebars', handlebars.engine);
app.set('view engine', 'handlebars');
app.set('port', process.env.PORT || 3000);

//enabling access to static files
app.use(express.static(__dirname + '/public'));
//enabling access to bootstrap
app.use('/scripts', express.static(__dirname + '/node_modules/bootstrap/dist/'));

//ROUTES
app.get('/', function(req, res){
  res.render('home');
});

//MIDDLEWARE - CATCH ALL SECTION
app.use(function(req, res){
  res.status(404);
  res.render('404');
});

//START SERVER
app.listen(app.get('port'), function(){
  console.log('Express started on localhost:' + app.get('port') + '; press Ctrl-C to terminate');
});
