var express = require('express');
var handlebars = require('express3-handlebars').create({defaultLayout:'main'});
var app = express();
var sayit = require('./lib/sayit.js');
app.disable('x-powered-by');
app.engine('handlebars', handlebars.engine);
app.set('view engine', 'handlebars');
app.set('port', process.env.PORT || 3000);
app.use(express.static(__dirname + '/public'));

app.use(function(req, res, next){
  res.locals.showTests = app.get('env') !== 'production' &&
  req.query.test === '1';
  next();
});

//ROUTES FOR GET
//app.get() is a method by which we are adding routes
app.get('/', function(req, res){
  res.render('home');
});

app.get('/headers', function(req, res){
  res.set('Content-Type', 'text/plain');
  var s = '';
  for(var name in req.headers){
    s += name + ': ' + req.headers[name] + '\n';
  }
  res.send(s);
});

app.get('/about', function(req, res){
  res.render('about', {saying: sayit.getSaying(), pageTestScript:'/qa/tests-about.js'});
});

app.get('/places/merse-dom', function(req, res){
  res.render('places/merse-dom');
});

app.get('/places/request-visit', function(req, res){
  res.render('places/request-visit');
});

//MIDDLEWARE - catch-all handler for everything
//custom 404 page
app.use(function(req, res){
  res.status(404);
  res.render('404');
});

app.use(function(err, req, res, next){
  console.error(err.stack);
  console.status(500);
  console.render('500');
});

app.listen(app.get('port'), function(){
  console.log('Express started on localhost' + app.get('port') + '; press Ctrl-C to terminate');
});
