var Browser = require('zombie');
var assert = require('chai').assert;

var browser;
//mocha -u tdd -R spec qa/tests-crosspage.js 2>/dev/null
suite('Cross-Page Testing', function(){
  setup(function(){
    browser = new Browser();
  });

  test('requesting a request-visit from the merse-dom should populate the ' +
  'referrer field', function(done){
    var referrer = 'http://localhost:3000/places/merse-dom';
    browser.visit(referrer, function(){
      browser.clickLink('.request-visit', function(){
        assert(browser.field('referrer').value === referrer);
        done();
      });
      done();
    });
  });

  test('visiting the "request group rate" page dirctly should result ' +
    'in an empty referrer field', function(done){
      browser.visit('http://localhost:3000/tours/request-group-rate',function(){
        assert(browser.field('referrer').value === '');
        done();
      });
      done();
    });
});
