var sayit = require('../lib/sayit.js');
var expect = require('chai').expect;
//mocha -u tdd -R spec qa/tests-unit.js
suite('Sayit tests', function(){
  test('getSaying() should return a string', function(){
    expect(typeof(sayit.getSaying()) === "string");
  });
});
