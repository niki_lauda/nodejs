function callback(req, res){
  res.writeHead(200, {'Content-Type':'text/plain'});
  res.end('Hello World!');
}

var http = require('http');
http.createServer(callback).listen(4020);

console.log('Server started on localhost:4020; press Ctrl-C to terminate...');
